//
//  ViewController.swift
//  AppDemo
//
//  Created by Wang, Shun on 16/8/15.
//  Copyright © 2016年 Wang, Shun. All rights reserved.
//

import UIKit
import Alamofire

class ViewController: UIViewController {

    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var requestIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    var task : URLSessionTask!
    
    @IBAction func requestAction(_ sender: AnyObject) {
//        let url = URL(string: "http://10.78.146.114:8080/hi")!
//        let request = URLRequest(url: url)
//        
//        let session = URLSession(configuration: URLSessionConfiguration())
//        
//        task = session.dataTask(with: url)
//        
//        task.response =
//        
//        task.resume()
        
        if (self.inputTextField.text?.isEmpty)! {
            let alert = UIAlertController.init(title: "Alert", message: "You need input something.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.navigationController?.present(alert, animated: true, completion: nil)
            return
        }
        
        self.requestIndicator.startAnimating()
        self.resultLabel.text = ""
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            self.sendRequest()
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func sendRequest() {
        let request = Alamofire.request("http://10.78.145.150:8080/\(inputTextField.text?.lowercased() ?? "")", withMethod: .get)
        request.responseString { response in
            if let result = response.result.value {
                self.resultLabel.text = result
            }else {
                self.resultLabel.text = "Nothing"
            }
            self.requestIndicator.stopAnimating()
        }
    }
}

