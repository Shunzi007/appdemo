//
//  HoneywellEULAViewController.h
//  MyAnalyzer
//
//  Created by Kris on 6/10/15.
//  Copyright (c) 2015 Honeywell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HONEULAViewController : UIViewController

@property (nonatomic, strong) NSString *appName;
@property (nonatomic, strong) UIViewController *didAgreeViewController;
@property (nonatomic, copy) void (^userDidAcceptBlock)();

- (IBAction)agreeAndContinueBottonTapped:(UIButton *)sender;

@end
