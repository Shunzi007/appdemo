//
//  HONEULAViewController.m
//
//  Created by Kris on 6/10/15.
//  Copyright (c) 2015 Honeywell. All rights reserved.
//

//############################################################################################//
//  Setup Instructions:
//
//  1) Add EULA View Controller files to project file.
//
//      Ensure eula.rtf is up to date (current version from June 11 2015)
//
//  2) In the AppDelegate import the HONEULAViewController header file
//
//      #import "HONEULAViewController.h"
//
//  3) Inside the application:didFinishLaunchingWithOptions: instantiate an instance of the
//  EULAView Controller and set it as the root view controller. This should be done after
//  a check to see if the EULA has been displayed and agreed to previously.
//
//      HONEULAViewController *eulaVC = [[HONEULAViewController alloc] init];
//      self.window.rootViewController = eulaVC;
//      [self.window makeKeyAndVisible];
//
//  4) Additionally you will need to set the view controller that should be displayed after the
//  user has accepted the EULA. This view controller will become the root view controller once
//  the EULA view controller has been dismissed
//
//      UINavigationController *navController = [[UINavigationController alloc]
//                                              initWithNibName:@"MainNavigationController" bundle:nil];
//      eulaVC.didAgreeViewController = navController;
//
//  5) You can pass a block to the eula view controller that will be called when the user agrees to the eula.
//  This can be used to record the action in the user default for example:
//
//          void (^acceptBlock)(void) = ^{
//              NSLog(@"Accept recorded");
//              [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"eulaAccepted"];
//              [[NSUserDefaults standardUserDefaults] synchronize];
//          };
//
//          eulaVC.userDidAcceptBlock = acceptBlock;
//
//  6) The view controller will automatically detect the project name,
//  however this can be optionally overridden
//
//      eulaVC.appName = @"Weather Information Service";
//
//  7) To view just the EULA from another view controller
//
//      HONLicenseViewController *licenseVC = [[HONLicenseViewController alloc] init];
//      licenseVC.appName = @"My App Name"; //optional
//      [self presentViewController:licenseVC animated:YES completion:nil];
//
//############################################################################################//

#import "HONEULAViewController.h"
#import "HONLicenseViewController.h"

@interface HONEULAViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *licenseAgreementButton;
@property (weak, nonatomic) IBOutlet UIView *appLabelContainerView;

@end

@implementation HONEULAViewController

-(id)init {
    self = [super initWithNibName:@"HONEULAViewController" bundle:nil];
    if (self) {
        // Do any additional setup after loading the view from its nib.
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Docs call for ##428bca, converted to RGB
    
    NSBundle *bundle = [NSBundle bundleForClass:[HONEULAViewController class]];
    NSURL *url = [bundle URLForResource:@"HONEULA" withExtension:@"bundle"];
    NSBundle *sourceBundle = [NSBundle bundleWithURL:url];
    UIImage* logoImage = [UIImage imageWithContentsOfFile:[sourceBundle pathForResource:@"honeywell_logo_white" ofType:@"png"]];
    self.logoImageView.image = logoImage;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self.iconView.image = [UIImage imageNamed:@"AppIcon76x76"];
    }
    else {
        self.iconView.image = [UIImage imageNamed:@"AppIcon60x60"];
    }
    
    NSMutableAttributedString *licenseBtnTitle = [[NSMutableAttributedString alloc] initWithString:@"License Agreement."];
    
    [licenseBtnTitle addAttribute:NSUnderlineStyleAttributeName
                          value:[NSNumber numberWithInteger:NSUnderlineStyleSingle]
                          range:NSMakeRange(0, [licenseBtnTitle length])];
    
    NSDictionary *attrs = @{NSUnderlineStyleAttributeName: [NSNumber numberWithInteger:NSUnderlineStyleSingle],
                            NSForegroundColorAttributeName: [UIColor whiteColor]};
    
    [licenseBtnTitle addAttributes:attrs range:NSMakeRange(0, [licenseBtnTitle length])];
    
    [self.licenseAgreementButton setAttributedTitle:licenseBtnTitle forState:UIControlStateNormal];
    if (self.appName) {
        [self updateAppNameLabel:self.appName];
    }
    else {
        NSBundle *bundle = [NSBundle mainBundle];
        NSDictionary *info = [bundle infoDictionary];
        NSString *prodName = [info objectForKey:(NSString *)kCFBundleNameKey];
        [self updateAppNameLabel:prodName];
    }
    
    [self setNeedsStatusBarAppearanceUpdate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setAppName:(NSString *)appName
{
    _appName = appName;
    [self updateAppNameLabel:appName];
}

-(void) updateAppNameLabel: (NSString *) appName
{
    self.appNameLabel.text = appName;
    [self.appNameLabel sizeToFit];
    [self.appLabelContainerView sizeToFit];
}

- (IBAction)agreeAndContinueBottonTapped:(UIButton *)sender
{
    if (self.didAgreeViewController) {
        self.didAgreeViewController.view.alpha = 0;
        [UIView animateWithDuration:0.25
                         animations:^{
                             self.view.alpha = 0;
                         } completion:^(BOOL finished) {
                            [[UIApplication sharedApplication] delegate].window.rootViewController = self.didAgreeViewController;
                             [[[UIApplication sharedApplication] delegate].window makeKeyAndVisible];
                             [UIView animateWithDuration:0.25 animations:^{
                                 self.didAgreeViewController.view.alpha = 1;
                             }];
                         }];
        
    }
    else {
        NSLog(@"*****ERROR***** Did not identify view controller to use when user agrees to EULA");
    }
    if (self.userDidAcceptBlock) {
        self.userDidAcceptBlock();
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showLicense"]) {
        [segue.destinationViewController setValue:self.appNameLabel.text forKey:@"appName"];
    }
}

#pragma mark - Status Bar Style
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
