//
//  HONLicenseViewController.m
//
//  Created by Kris on 6/10/15.
//  Copyright (c) 2015 Honeywell. All rights reserved.
//

#import "HONLicenseViewController.h"
#import "HONEULAViewController.h"

@interface HONLicenseViewController ()
@property (weak, nonatomic) IBOutlet UITextView *licenseTextView;
@property (weak, nonatomic) IBOutlet UILabel *appNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *agreeButton;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@end

@implementation HONLicenseViewController

-(id)init {
    self = [super initWithNibName:@"HONLicenseViewController" bundle:nil];
    if (self) {
        // Do any additional setup after loading the view from its nib.
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Docs call for ##428bca, converted to RGB
    [self.agreeButton setBackgroundColor:[UIColor colorWithRed:0.26 green:0.57 blue:0.79 alpha:1.0]];
    [self.agreeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self setNeedsStatusBarAppearanceUpdate];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSBundle *bundle = [NSBundle bundleForClass:[HONEULAViewController class]];
    NSURL *url = [bundle URLForResource:@"HONEULA" withExtension:@"bundle"];
    NSBundle *sourceBundle = [NSBundle bundleWithURL:url];
    UIImage* logoImage = [UIImage imageWithContentsOfFile:[sourceBundle pathForResource:@"honeywell_logo_red" ofType:@"png"]];
    self.logoImageView.image = logoImage;
    
    NSString *theAppName;
    if (self.appName != nil) {
        theAppName = self.appName;
    }
    else {
        NSBundle *bundle = [NSBundle mainBundle];
        NSDictionary *info = [bundle infoDictionary];
        NSString *prodName = [info objectForKey:(NSString *)kCFBundleNameKey];
        theAppName = prodName;
    }
    self.appNameLabel.text = theAppName;
    
    
    
    NSURL *eulaFileURL = [NSURL fileURLWithPath:[sourceBundle pathForResource:@"eula" ofType:@"rtf"]];
    NSMutableAttributedString *eulaText = [[NSMutableAttributedString alloc]
                                           initWithFileURL: eulaFileURL
                                           options:@{NSDocumentTypeDocumentAttribute: NSRTFTextDocumentType}
                                           documentAttributes:nil
                                           error:nil];
    [[eulaText mutableString] replaceOccurrencesOfString:@"XXX"
                                              withString:self.appNameLabel.text.uppercaseString
                                                 options:NSLiteralSearch
                                                   range:NSMakeRange(0, eulaText.length)];
    self.licenseTextView.attributedText = eulaText;
    self.licenseTextView.layer.borderColor = [UIColor blackColor].CGColor;
    self.licenseTextView.layer.borderWidth = 1.0;
    self.licenseTextView.layer.cornerRadius = 4;
    self.licenseTextView.scrollsToTop = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - button handling
- (IBAction)backButtonTapped:(UIBarButtonItem *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)agreeAndContinueButtonTapped:(id)sender {
    
    if ([self.presentingViewController isKindOfClass:[HONEULAViewController class]]) {
        [((HONEULAViewController *)self.presentingViewController) agreeAndContinueBottonTapped:sender];
    }
    else {
        [self backButtonTapped:sender];
    }
}

#pragma mark - Status Bar Style
- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
