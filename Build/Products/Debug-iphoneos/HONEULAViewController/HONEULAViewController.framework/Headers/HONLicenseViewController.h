//
// HONLicenseViewController.h
//
//  Created by Kris on 6/10/15.
//  Copyright (c) 2015 Honeywell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HONLicenseViewController : UIViewController

@property (nonatomic, strong) NSString *appName;

@end
